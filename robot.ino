#include <Servo.h>
#define DEBUG true
// Pinii motor 1
#define mpin00 5
#define mpin01 6
// Pinii motor 2
#define mpin10 3
#define mpin11 11
const int TRIG_PIN = 9;
const int ECHO_PIN = 10;
int startRobot = 0;
Servo srv;
void do_mesure(int pin);
void setup() {
  Serial.begin(115200);
  sendData("AT+RST\r\n", 2000, false); // resetare modul
  sendData("AT+CWSAP=\"oricenume\",\"12345678\",1,2,4,0\r\n", 2000, DEBUG);
  sendData("AT+CWMODE=2\r\n", 1000, false); // configurare ca access point
  sendData("AT+CIFSR\r\n", 1000, DEBUG); // citeste adresa IP
  sendData("AT+CWSAP?\r\n", 2000, DEBUG); // citeste informatia SSID (nume retea)
  sendData("AT+CIPMUX=1\r\n", 1000, false); // configurare conexiuni multiple
  sendData("AT+CIPSERVER=1,80\r\n", 1000, false); // pornire server pe port 80

  pinMode(TRIG_PIN, OUTPUT);
  digitalWrite(TRIG_PIN, 0);
  pinMode(ECHO_PIN, INPUT);
  digitalWrite(mpin00, 0);
  digitalWrite(mpin01, 0);
  digitalWrite(mpin10, 0);
  digitalWrite(mpin11, 0);
  pinMode (mpin00, OUTPUT);
  pinMode (mpin01, OUTPUT);
  pinMode (mpin10, OUTPUT);
  pinMode (mpin11, OUTPUT);
  playWithServo(8);
  float dist = calcDist(TRIG_PIN, ECHO_PIN);
  pinMode(13, OUTPUT);
}
// Functie pentru controlul unui motor
// Intrare: pinii m1 si m2, directia si viteza
void StartMotor (int m1, int m2, int forward, int speed)
{

  if (speed == 0) // oprire
  {
    digitalWrite(m1, 0);
    digitalWrite(m2, 0);
  }
  else
  {
    if (forward)
    {
      digitalWrite(m2, 0);
      analogWrite (m1, speed); // folosire PWM
    }
    else
    {
      digitalWrite(m1, 0);
      analogWrite(m2, speed);
    }
  }
}
// Functie de siguranta
// Executa oprire motoare, urmata de delay
void delayStopped(int ms)
{
  StartMotor (mpin00, mpin01, 0, 0);
  StartMotor (mpin10, mpin11, 0, 0);
  delay(ms);
}
void playWithServo(int pin)
{
  srv.attach(pin);
  srv.write(0);
  delay(1000);
  srv.write(90);
  delay(1000);
  srv.detach();
}
int rotateServo(int pin) {
  float direct[2];
  srv.attach(pin);
  srv.write(0);
  direct[0] = calcDist(TRIG_PIN, ECHO_PIN);
  delay(1000);
  srv.write(180);
  direct[1] = calcDist(TRIG_PIN, ECHO_PIN);
  delay(1000);
  srv.write(90);
  delay(1000);
  srv.detach();
  int dir = 0;
  if (direct[1] > direct[0]) {
    dir = 1;
  }

  return dir;
}
unsigned long startTime = 0, endTime = 0;
void loop() {
  if (Serial.available()) {
    if (Serial.find("+IPD,")) {
      delay(500);
      int connectionId = Serial.read() - 48; // functia read() returneaza valori zecimale ASCII
      // si caracterul ‘0’ are codul ASCII 48
      String webpage = "<h1>Hello World!</h1><a href=\"/l0\"><button>PLUS</button></a>";
      String cipSend = "AT+CIPSEND=";
      cipSend += connectionId;
      cipSend += ",";
      webpage += "<a href=\"/l1\"><button>MINUS</button></a>";
      cipSend += webpage.length();
      cipSend += "\r\n";
      sendData(cipSend, 100, DEBUG);
      sendData(webpage, 150, DEBUG);

      String closeCommand = "AT+CIPCLOSE=";
      closeCommand += connectionId; //se adauga identificatorul conexiunii
      closeCommand += "\r\n";
      sendData(closeCommand, 300, DEBUG);
    }
  }
  digitalWrite(13, 1);
  delay(10);
  endTime = millis();

  float distCm = calcDist(TRIG_PIN, ECHO_PIN);
  if (startRobot == 1) {
    controlRobot(distCm);
  } else {
    StartMotor (mpin00, mpin01, 0, 0);
    StartMotor (mpin10, mpin11, 0, 0);
  }

}
void controlRobot(float dist) {
  if (dist >= 20.0) {
    if (millis() - startTime > 4000) {
      StartMotor (mpin00, mpin01, 0, 0);
      StartMotor (mpin10, mpin11, 0, 0);
      do_mesure(8);
      startTime = endTime;
    }
    StartMotor (mpin00, mpin01, 0, 128);
    StartMotor (mpin10, mpin11, 0, 128);
  } else {
    StartMotor (mpin00, mpin01, 0, 0);
    StartMotor (mpin10, mpin11, 0, 0);
    int direct = rotateServo(8);
    switch (direct) {
      case 0:
        StartMotor (mpin00, mpin01, 1, 128);
        StartMotor (mpin10, mpin11, 0, 128);
        delay(500);
        break;
      case 1:
        StartMotor (mpin00, mpin01, 0, 128);
        StartMotor (mpin10, mpin11, 1, 128);
        delay(500);
        break;
      default:
        StartMotor (mpin00, mpin01, 0, 0);
        StartMotor (mpin10, mpin11, 0, 0);

    }
    StartMotor (mpin00, mpin01, 0, 0);
    StartMotor (mpin10, mpin11, 0, 0);
  }
}
float calcDist(int trg, int echo)
{
  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;
  float cm;
  digitalWrite(trg, HIGH);
  delayMicroseconds(10);
  digitalWrite(trg, LOW);
  while ( digitalRead(echo) == 0 );
  t1 = micros();
  while ( digitalRead(echo) == 1);
  t2 = micros();
  pulse_width = t2 - t1;
  cm = pulse_width / 58.0;
  return cm;
}
String sendData(String command, const int timeout, boolean debug) {
  String response = "";
  //Serial.println(command);

  Serial.print(command); // trimite comanda la esp8266
  long int time = millis();
  while ((time + timeout) > millis()) {
    while (Serial.available()) {
      char c = Serial.read(); // citeste caracter urmator
      response += c;
    }
  }
  if (response.indexOf("/l0") != -1) {
    startRobot = 1;
  }
  if (response.indexOf("/l1") != -1) {
    startRobot = 0;
  }
  return response;
}
void do_mesure(int pin)
{
  srv.attach(pin);
  srv.write(0);
  float distDreapta = calcDist(TRIG_PIN, ECHO_PIN);
  delay(1000);
  srv.write(90);
  float distFata = calcDist(TRIG_PIN, ECHO_PIN);
  delay(1000);
  srv.write(180);
  float distStanga = calcDist(TRIG_PIN, ECHO_PIN);
  delay(1000);
  srv.write(90);
  delay(1000);
  srv.detach();
  String masuratori = "fata=";
  masuratori += distFata;
  masuratori += " stanga=";
  masuratori += distStanga;
  masuratori += " dreapta=";
  masuratori += distDreapta;
  Serial.println(masuratori);
}
